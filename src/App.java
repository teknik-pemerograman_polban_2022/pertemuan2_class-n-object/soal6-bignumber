
// Importing the BigInteger and Scanner classes from the java.math and java.util packages.
import java.math.BigInteger;
import java.util.Scanner;

public class App {

    /**
     * The main method of the program.
     * 
     * @param args unused
     * @throws Exception unused
     */
    public static void main(String[] args) throws Exception {
        // Taking two numbers from the user and adding them together and multiplying
        // them together.
        Scanner kScanner = new Scanner(System.in);
        BigInteger A = new BigInteger(kScanner.nextLine());
        BigInteger B = new BigInteger(kScanner.nextLine());
        System.out.println(A.add(B));
        System.out.println(A.multiply(B));
    }
}
